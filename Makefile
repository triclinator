triclinator: main.o crystal.o util.o
	gcc main.o crystal.o util.o -o triclinator -lgsl -lgslcblas -lm

test: test.o crystal.o util.o
	gcc test.o crystal.o util.o -o test -lm

main.o:	main.c
	gcc -g -c -I/usr/include main.c -o main.o

crystal.o: crystal.c
	gcc -g -c -I/usr/include crystal.c -o crystal.o

util.o: util.c
	gcc -g -c -I/usr/include util.c -o util.o

clean:
	rm -f triclinator test main.o crystal.o util.o test.o

.PHONY: clean
