/*
 * util.h
 *
 * Utility stuff
 *
 * (c) 2009 Thomas White <taw27@cam.ac.uk>
 *
 * Triclinator - solve nasty triclinic unit cells
 *
 */

#ifndef UTIL_H
#define UTIL_H

typedef struct
{
	signed int h1;
	signed int k1;
	signed int l1;

	signed int h2;
	signed int k2;
	signed int l2;	/* Or zero */

	double meas;
	double esd;
} MVal;

extern double read_value(const char *text);
extern int is_dspacing(MVal val);

#endif	/* UTIL_H */
