/*
 * test.c
 *
 * Unit tests
 *
 * (c) 2009 Thomas White <taw27@cam.ac.uk>
 *
 * Triclinator - solve nasty triclinic unit cells
 *
 */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "crystal.h"

int main(void)
{
	Cell cell;

	cell.a = 1.0;
	cell.b = 1.0;
	cell.c = 1.0;
	cell.al = DEG2RAD(90.0);
	cell.be = DEG2RAD(120.0);
	cell.ga = DEG2RAD(90.0);

	printf("%8.5f deg\n", RAD2DEG(plane_angle(cell, 0, 0, 1,  0, 1, 0)));

	return 0;
}
