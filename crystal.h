/*
 * crystal.h
 *
 * Crystallographic stuff
 *
 * (c) 2009 Thomas White <taw27@cam.ac.uk>
 *
 * Triclinator - solve nasty triclinic unit cells
 *
 */

#ifndef CRYSTAL_H
#define CRYSTAL_H

#define RAD2DEG(a) ((a)*180/M_PI)
#define DEG2RAD(a) ((a)*M_PI/180)

#include "util.h"

typedef struct
{
	double a;
	double b;
	double c;
	double al;
	double be;
	double ga;
} Cell;

/* Return what the measurement 'val' would have been if the cell were 'cell' */
extern double crystal_calc(MVal val, Cell cell);
extern double dspacing(Cell cell, double h, double k, double l);
extern double plane_angle(Cell cell, double h1, double k1, double l1,
				   double h2, double k2, double l2);

#endif	/* CRYSTAL_H */
