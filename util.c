/*
 * util.c
 *
 * Utility stuff
 *
 * (c) 2009 Thomas White <taw27@cam.ac.uk>
 *
 * Triclinator - solve nasty triclinic unit cells
 *
 */

#include <stdio.h>

#include "util.h"

double read_value(const char *text)
{
	while ( 1 ) {

		float d;
		char buf[64];

		printf("%s", text);
		if ( fgets(buf, 63, stdin) != buf ) {
			fprintf(stderr, "Error reading from input\n");
		}
		if ( sscanf(buf, "%f", &d) != 1 ) {
			printf("Invalid input, try again.\n");
		} else {
			return d;
		}
	}
}

int is_dspacing(MVal val)
{
	if ( (val.h2 == 0) && (val.k2 == 0) && (val.l2 == 0) ) return 1;
	return 0;
}
